RELEASE=3.1

PACKAGE=libusb-1.0-0
PKGVERSION=1.0.17
PKGRELEASE=1

PKGDIR=libusbx-${PKGVERSION}
PKGSRC=libusbx_${PKGVERSION}.orig.tar.bz2
DEBSRC=libusbx_${PKGVERSION}-${PKGRELEASE}.debian.tar.xz

ARCH:=$(shell dpkg-architecture -qDEB_BUILD_ARCH)

DEBS=								\
	${PACKAGE}_${PKGVERSION}-${PKGRELEASE}_${ARCH}.deb	\
	${PACKAGE}-dev_${PKGVERSION}-${PKGRELEASE}_${ARCH}.deb

all: ${DEBS}
	echo ${DEBS}

${DEBS}: ${PKGSRC}
	echo ${DEBS}
	rm -rf ${PKGDIR} debian
	tar xf ${PKGSRC}
	tar xf ${DEBSRC}
	cp -a debian ${PKGDIR}/debian
	cd ${PKGDIR}; dpkg-buildpackage -rfakeroot -b -us -uc


.PHONY: upload
upload: ${DEBS}
	umount /pve/${RELEASE}; mount /pve/${RELEASE} -o rw
	mkdir -p /pve/${RELEASE}/extra
	rm -f /pve/${RELEASE}/extra/Packages*
	rm -f /pve/${RELEASE}/extra/${PACKAGE}1_*.deb
	rm -f /pve/${RELEASE}/extra/${PACKAGE}-dev_*.deb
	cp ${DEBS} /pve/${RELEASE}/extra
	cd /pve/${RELEASE}/extra; dpkg-scanpackages . /dev/null > Packages; gzip -9c Packages > Packages.gz
	umount /pve/${RELEASE}; mount /pve/${RELEASE} -o ro

distclean: clean

.PHONY: clean
clean:
	rm -rf *~ debian *.deb *.udeb *.changes *.dsc ${PKGDIR}

.PHONY: dinstall
dinstall: ${DEBS}
	dpkg -i ${DEBS}
